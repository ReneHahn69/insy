-- phpMyAdmin SQL Dump
-- version 4.4.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Erstellungszeit: 15. Jun 2016 um 14:40
-- Server-Version: 5.6.26
-- PHP-Version: 5.6.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Datenbank: `carsharing`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `administrator`
--

CREATE TABLE IF NOT EXISTS `administrator` (
  `id` int(11) NOT NULL,
  `Name` varchar(45) NOT NULL,
  `Password` varchar(45) DEFAULT NULL,
  `Lager_idLager` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `administrator`
--

INSERT INTO `administrator` (`id`, `Name`, `Password`, `Lager_idLager`) VALUES
(1, 'admin', 'admin', NULL);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `bestand`
--

CREATE TABLE IF NOT EXISTS `bestand` (
  `idBestand` int(11) NOT NULL,
  `idAuto` int(11) NOT NULL,
  `AusgangsDatum` date NOT NULL,
  `EingangsDatum` date NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `bestand`
--

INSERT INTO `bestand` (`idBestand`, `idAuto`, `AusgangsDatum`, `EingangsDatum`) VALUES
(7, 1, '2016-06-01', '2016-06-15'),
(8, 2, '2016-06-15', '2016-06-15'),
(11, 3, '2016-06-15', '2016-06-15');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `fahrzeuge`
--

CREATE TABLE IF NOT EXISTS `fahrzeuge` (
  `idFahrzeug` int(11) NOT NULL,
  `Bezeichnung` varchar(50) NOT NULL,
  `Status` varchar(45) NOT NULL,
  `Standort` varchar(45) NOT NULL,
  `Bestand_idBestand` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `fahrzeuge`
--

INSERT INTO `fahrzeuge` (`idFahrzeug`, `Bezeichnung`, `Status`, `Standort`, `Bestand_idBestand`) VALUES
(1, 'VW Golf', 'Frei', 'Wien', NULL),
(2, 'Audi A4', 'Frei', 'Salzburg', NULL),
(3, 'BMW 3er', 'Frei', 'Linz', NULL),
(4, 'Skoda', 'Frei', 'Wien', NULL),
(6, 'VW Golf', 'Frei', 'Graz', NULL);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `idUser` int(11) NOT NULL,
  `username` varchar(45) NOT NULL,
  `password` varchar(45) NOT NULL,
  `Fahrzeuge_idFahrzeug` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `user`
--

INSERT INTO `user` (`idUser`, `username`, `password`, `Fahrzeuge_idFahrzeug`) VALUES
(1, 'rene', 'rene', NULL);

--
-- Indizes der exportierten Tabellen
--

--
-- Indizes für die Tabelle `administrator`
--
ALTER TABLE `administrator`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `Name_UNIQUE` (`Name`),
  ADD KEY `fk_Administrator_Lager1_idx` (`Lager_idLager`);

--
-- Indizes für die Tabelle `bestand`
--
ALTER TABLE `bestand`
  ADD PRIMARY KEY (`idBestand`),
  ADD UNIQUE KEY `idWaren_UNIQUE` (`idAuto`);

--
-- Indizes für die Tabelle `fahrzeuge`
--
ALTER TABLE `fahrzeuge`
  ADD PRIMARY KEY (`idFahrzeug`),
  ADD KEY `fk_Fahrzeuge_Bestand1_idx` (`Bestand_idBestand`);

--
-- Indizes für die Tabelle `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`idUser`),
  ADD UNIQUE KEY `username_UNIQUE` (`username`),
  ADD KEY `fk_User_Fahrzeuge1_idx` (`Fahrzeuge_idFahrzeug`);

--
-- AUTO_INCREMENT für exportierte Tabellen
--

--
-- AUTO_INCREMENT für Tabelle `administrator`
--
ALTER TABLE `administrator`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT für Tabelle `bestand`
--
ALTER TABLE `bestand`
  MODIFY `idBestand` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT für Tabelle `fahrzeuge`
--
ALTER TABLE `fahrzeuge`
  MODIFY `idFahrzeug` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT für Tabelle `user`
--
ALTER TABLE `user`
  MODIFY `idUser` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- Constraints der exportierten Tabellen
--

--
-- Constraints der Tabelle `administrator`
--
ALTER TABLE `administrator`
  ADD CONSTRAINT `fk_Administrator_Lager1` FOREIGN KEY (`Lager_idLager`) REFERENCES `bestand` (`idBestand`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints der Tabelle `fahrzeuge`
--
ALTER TABLE `fahrzeuge`
  ADD CONSTRAINT `fk_Fahrzeuge_Bestand1` FOREIGN KEY (`Bestand_idBestand`) REFERENCES `bestand` (`idBestand`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints der Tabelle `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `fk_User_Fahrzeuge1` FOREIGN KEY (`Fahrzeuge_idFahrzeug`) REFERENCES `fahrzeuge` (`idFahrzeug`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
