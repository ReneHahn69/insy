﻿<!DOCTYPE html>
<html lang="en">
<?php
include "db.php";
?>
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>SB Admin 2 - Bootstrap Admin Theme</title>

    <!-- Bootstrap Core CSS -->
    <link href="startbootstrap-sb-admin-2-1.0.8/bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="startbootstrap-sb-admin-2-1.0.8/bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- Timeline CSS -->
    <link href="startbootstrap-sb-admin-2-1.0.8/dist/css/timeline.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="startbootstrap-sb-admin-2-1.0.8/dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="startbootstrap-sb-admin-2-1.0.8/bower_components/morrisjs/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="startbootstrap-sb-admin-2-1.0.8/bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="administrator_nav.php">SB Admin v2.0</a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
                <!-- /.dropdown -->
               
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="logout.php"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->
        </nav>

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Dashboard</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-4 col-md-6">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-comments fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge"><?php
									$erg = $db->query('Select count(idFahrzeug) from fahrzeuge');
									$a = $erg->fetch(PDO::FETCH_ASSOC);    
									echo'<p>'.$a['count(idFahrzeug)'].'</p>';?></div>
                                    <div>Löschen & bearbeiten</div>
                                </div>
                            </div>
                        </div>
                        <a href="admin_delete.php">
                            <div class="panel-footer">
                                <span class="pull-left">View Details</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6">
                    <div class="panel panel-green">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-tasks fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge"></div>
                                    <div>Hinzufügen</div>
                                </div>
                            </div>
                        </div>
                        <a href="admin_add.php">
                            <div class="panel-footer">
                                <span class="pull-left">View Details</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6">
                    <div class="panel panel-yellow">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-shopping-cart fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge"></div>
                                    <div>Statistik</div>
                                </div>
                            </div>
                        </div>
                        <a href="#">
                            <div class="panel-footer">
                                <span class="pull-left">View Details</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
				</div>
						<div class="row">
                <div class="col-lg-8">
				<div class="panel panel-default">
				<div class="panel-heading">
                            <i class="fa fa-bar-chart-o fa-fw"></i> Rückgabe
							 <div class="pull-right">							
                            </div>
                  <div class="panel-body">
				  <div id="morris-area-chart"></div>
                           
                                        <table class="table table-bordered table-hover table-striped">
                                           <?php
													
												//Anzeige Motor
												$erg = $db->query('SELECT idAuto,AusgangsDatum,EingangsDatum from bestand ORDER BY idAuto');
											?>
										   <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Ausleih Datum</th>
                                                    <th>Rückgabe Datum</th>
                                                </tr>
                                            </thead>
											<?php
											foreach ($erg as $row)
											{
                                            echo'<tbody>
                                                <tr>
                                                    <td>'.$row['idAuto'].'</td>
                                                    <td>'.$row['AusgangsDatum'].'</td>
                                                    <td>'.$row['EingangsDatum'].'</td>
                                                </tr>
                                              </tbody>';
											  }
											  ?>
                                        </table>
                                    </div>
                                    <!-- /.table-responsive -->
                                </div>
                                <!-- /.col-lg-4 (nested) -->
                               
                                <!-- /.col-lg-8 (nested) -->
                            </div>
                            <!-- /.row -->
                        </div>
						</div>
					
				
						<div class="row">
                <div class="col-lg-8">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-bar-chart-o fa-fw"></i> Autos frei/belegt
                            <div class="pull-right">							
                            </div>
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div id="morris-area-chart"></div>
							
							 <table class="table table-bordered table-hover table-striped">
							<thead>
							<?php  $erg = $db->query('Select count(idFahrzeug) from fahrzeuge where Status="Frei"'); 
							$a = $erg->fetch(PDO::FETCH_ASSOC);
							$erg = $db->query('Select count(idFahrzeug) from fahrzeuge where Status="Belegt"');
							$b = $erg->fetch(PDO::FETCH_ASSOC);?>
		
                                                <tr>
                                                    <th>Freie Autos</th>
                                                    <th>Autos belegt</th>
                                                </tr>
                                            </thead>
											
											
											<?php
											
											
                                                echo'<tbody>
												<tr>
													<td><p>'.$a['count(idFahrzeug)'].'<p></td>
                                                    <td><p>'.$b['count(idFahrzeug)'].'<p></td>
                                                    
                                                </tr>
												
												</tbody>';

												?>
												
                                              					  
							</table>
							</form>
                        </div>
                        <!-- /.panel-body -->
                    </div>
            </div>
            <!-- /.row -->
        
        <!-- /#page-wrapper -->
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="startbootstrap-sb-admin-2-1.0.8/bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="startbootstrap-sb-admin-2-1.0.8/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="startbootstrap-sb-admin-2-1.0.8/bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="startbootstrap-sb-admin-2-1.0.8/bower_components/raphael/raphael-min.js"></script>
    <script src="startbootstrap-sb-admin-2-1.0.8/bower_components/morrisjs/morris.min.js"></script>
    <script src="startbootstrap-sb-admin-2-1.0.8/js/morris-data.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="startbootstrap-sb-admin-2-1.0.8/dist/js/sb-admin-2.js"></script>

</body>

</html>
